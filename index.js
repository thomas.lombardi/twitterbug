
'use strict';


const chalk = require('chalk');
const fs = require('fs');
const Promise = require('bluebird');
let processing = require('./processing');
let bot = require('./bot');


//check if stats file exists
fs.access('./quoteSources/jitterbugFullText/jitterStatsFullText.json', fs.F_OK, function(err) {
	if(!err) {
		console.log(chalk.green('PROCEED'));
		botTime();
		//justMakeRandomQuotes();
	} else {
		processing.jitterbugFullProcessing().then((quote) => {
			console.log(chalk.green('HAVE BUILT STATS, PROCEED'))
			botTime();
		})
	}
});



function botTime() {
	
	bot.postSomeLoveliness();
	let minutes = 50 + getRandomNumberBetween0and10();
	let rightNow = getDateTime();
	console.log(chalk.magenta('making new post @ ', rightNow ))
	console.log(chalk.yellow('expect new post @ ', getDateTime(minutes)))
	
	
	setTimeout(botTime, minutes * 180 * 1000);
}


function getRandomNumberBetween0and10() {
	return Math.floor(Math.random() * 10);
}


function justMakeRandomQuotes() {
	var quoteMaker = require('./quotemaker/generateQuotes');
	var textStats = require('./quoteSources/jitterbugFullText/jitterStatsFullText');

	for(let i = 0; i < 10; i++) {
		quoteMaker.returnGeneratedQuote(textStats).then((newQuote) => {
			console.log(chalk.red(i, "\tlength => ") + chalk.green(newQuote.length + "\t") + chalk.blue.bold(newQuote));
		});
	}
}

function makeRandomQuotePlease() {
	var quoteMaker = require('./quotemaker/generateQuotes');
	var textStats = require('./quoteSources/jitterbugFullText/jitterStatsFullText');
	quoteMaker.returnGeneratedQuote(textStats).then((newQuote) => {
		console.log(chalk.red("length => ") + chalk.green(newQuote.length + "\t") + chalk.blue.bold(newQuote));
	});
}

function getDateTime(minutesToAdd) {

    var date = new Date();

    if(minutesToAdd)
    	date = new Date(date.getTime() + minutesToAdd*60000)

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;    

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;

}
