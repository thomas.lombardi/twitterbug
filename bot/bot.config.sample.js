'use strict';
module.exports = {
	CONSUMER_KEY:  			'',
	CONSUMER_SECRET: 		'',
	ACCESS_TOKEN: 			'',
	ACCESS_TOKEN_SECRET: 	'',
	TIMEOUT_MS : 			60*1000
}