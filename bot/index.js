'use strict';

var Twit = require('twit');
var config = require('./bot.config.js');
var chalk = require('chalk');

//LOGGING
var winston = require('winston');
winston.add(winston.transports.File, {filename: 'twitterlogs.log'});
winston.remove(winston.transports.Console);



var myLovelyBot = new Twit({
	consumer_key: 			config.CONSUMER_KEY,
	consumer_secret: 		config.CONSUMER_SECRET,
  	access_token:         	config.ACCESS_TOKEN,  	
  	access_token_secret:  	config.ACCESS_TOKEN_SECRET,
  	timeout_ms: 		  	config.TIMEOUT_MS
});




function postSomeLoveliness() {
	let quoteMaker = require('../quotemaker/generateQuotes');
	let textStats = require('../quoteSources/jitterbugFullText/jitterStatsFullText');

	quoteMaker.returnGeneratedQuote(textStats).then((newQuote) => {
		console.log(chalk.red("Here's the quote:   ") + chalk.blue.bold(newQuote));
		console.log(chalk.red("Quote length => ") + chalk.green(newQuote.length));

		myLovelyBot.post('statuses/update', { status: newQuote }, function(err, data, response) {

			if(err) { throw err; }

			//console.log(chalk.green("id: ", data.id, " :: created at: ", data.created_at, "text: ", data.text));
			winston.info("id: ", data.id, " :: created at: ", data.created_at, "text: ", data.text);
		})


	});
}



module.exports = {
	postSomeLoveliness
}