
'use strict';

function jitterbugFullProcessing() {

	// JITTERBUG FULL TEXT PROCESSING
	let buildStats = require('./quotemaker/buildWordStats');


	return new Promise((resolve) => {
		//declare your paths
		var fullText = './quoteSources/jitterbugFullText/jitterbugPerfume.txt';
		var fullTextArray = './quoteSources/jitterbugFullText/jitterArrayFullText.json';
		var fullTextStats = './quoteSources/jitterbugFullText/jitterStatsFullText.json';

		// make text into an array
		var textSource = require('./quotePuller/fromTextFile');
		textSource.makeHugeArrayFromFullText(fullText, fullTextArray)
		.then(() => {
			// map out word usage
			var textArray = require(fullTextArray);
			buildStats.buildWordModel(textArray, fullTextStats)
			.then(() => {
				// GENERATE QUOTES
				var quoteMaker = require('./quotemaker/generateQuotes');
				var textStats = require(fullTextStats)
				quoteMaker.returnGeneratedQuote(textStats).then((newQuote) => {
					console.log(chalk.red("Here's the quote:   ") + chalk.blue.bold(newQuote));
					console.log(chalk.red("Quote length => ") + chalk.green(newQuote.length));	
					resolve(newQuote);
				});
			});	
		});
	});
		
}


function huckFinnProcessing() {
	// HUCK FINN FULL TEXT PROCESSING

	return new Promise((resolve) => {
		//declare your paths
		var fullText = './quoteSources/huckFinn/huckFinn.txt';
		var fullTextArray = './quoteSources/huckFinn/huckFinnArray.json';
		var fullTextStats = './quoteSources/huckFinn/huckFinnStats.json';

		// make text into an array
		var textSource = require('./quotePuller/fromTextFile');
		textSource.makeHugeArrayFromFullText(fullText, fullTextArray)
		.then(() => {
				// map out word usage
			var textArray = require(fullTextArray);
			buildStats.buildWordModel(textArray, fullTextStats)
			.then(() => {
				// GENERATE QUOTES
				var quoteMaker = require('./quotemaker/generateQuotes');
				var textStats = require(fullTextStats)
				quoteMaker.returnGeneratedQuote(textStats).then((newQuote) => {
					console.log(chalk.red("Here's the quote:   ") + chalk.blue.bold(newQuote));
					console.log(chalk.red("Quote length => ") + chalk.green(newQuote.length));
					resolve(newQuote)
				});
			});
		});
	})

		
}

module.exports = {
	huckFinnProcessing,
	jitterbugFullProcessing
}