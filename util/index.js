'use strict';
//const fs = require('fs');
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));


function writeToFile(filename, string) {
	return new Promise((resolve) => {
		fs.writeFileAsync(filename, JSON.stringify(string), {}).then(() => {
	    	console.log('done writing file => ', filename);
	    	resolve();
	    });	
	})
    

}



module.exports = {
    writeToFile
}