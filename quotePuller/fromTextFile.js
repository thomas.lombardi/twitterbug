'use strict';

var fs = require('fs');
var readline = require('readline');
const chalk = require('chalk');
var utils = require('../util');
var Promise = require('bluebird')

function makeHugeArrayFromFullText(sourcePath, destinationPath) {

	return new Promise((resolve) => {
		fs.readFile(sourcePath, function(err, data) {
			if(err) {
				throw err;
			}

			var array = data.toString().split(".");
			console.log(chalk.green("This has approximately ", array.length, " sentences"));

			var onThePath = false;
			var currentLine = '';
			var perfumedArray = [];
			array.forEach((line) => {

				//check for empty line
				var emptyCheck = line.replace(/^\s+/, '').replace(/\s+$/, '');
				if(emptyCheck !== '') {
					//clean up line endings and white space
					var cleaned = line.replace(/(?:\r\n|\r|\n)/g, ' ').trim();
					perfumedArray.push(cleaned);
				} 
			});

			utils.writeToFile(destinationPath, perfumedArray).then(() => {
				resolve();	
			});	
		});
	})
	
}


function makeHugeArrayAlgernon() {
	//console.log('directory ', __dirname)

	fs.readFile('quoteSources/ImportanceOfBeingEarnest.txt', function(err, data) {
		if(err) {
			throw err;
		}

		var array = data.toString().split("\n");


		var onThePath = false;
		var currentLine = '';
		var algieArray = [];
		array.forEach((line) => {
			
			//check for empty line
			var emptyCheck = line.replace(/^\s+/, '').replace(/\s+$/, '');
			if(emptyCheck === '') {
				onThePath = false;
				if(currentLine.length > 4)
					algieArray.push(currentLine.trim());
				currentLine = '';
			} else {
				
				if(onThePath) {
					currentLine += line.replace(/ *\[[^\]]*]/, '') + ' ';
				} else {
					if (line.substring(0, 9) === 'Algernon.') {
						onThePath = true;
						currentLine += line.replace('Algernon.', '').replace(/ *\[[^\]]*]/, '') + ' ';
					}
				}
			}


		});

		algieArray.forEach((line) => {
			console.log(chalk.green(line));
		});

		utils.writeToFile('quoteSources/algernon.json', algieArray);

		
	});

}


function makeHugeArrayJitterbug() {
	fs.readFile('quoteSources/jitterbugFullText/jitterbugPerfume.txt', function(err, data) {
		if(err) {
			throw err;
		}

		var array = data.toString().split(".");
		console.log(chalk.green("Jitterbug Pefume has approximately ", array.length, " sentences"));

		var onThePath = false;
		var currentLine = '';
		var perfumedArray = [];
		array.forEach((line) => {

			//check for empty line
			var emptyCheck = line.replace(/^\s+/, '').replace(/\s+$/, '');
			if(emptyCheck !== '') {
				perfumedArray.push(line.trim())
			} 
		});


		utils.writeToFile('quoteSources/jitterbugFullText/jitterArrayFullText.json', perfumedArray);

		
	});
}






module.exports = {
	makeHugeArrayFromFullText,
	makeHugeArrayAlgernon,
	makeHugeArrayJitterbug	
}