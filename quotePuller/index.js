
'use strict';

var cheerio = require('cheerio');
var request = require('request');
var fs = require('fs');
var Promise = require('bluebird');

const firstUrl = 'https://www.goodreads.com/work/quotes/1105831-jitterbug-perfume?page=';



exports.buildQuotes = function() {
	var quotes= [];
	Promise.join(
		pullQuotes(1),
		pullQuotes(2),
		pullQuotes(3),
		pullQuotes(4),
		pullQuotes(5),
		pullQuotes(6),
		pullQuotes(7),
		function(one, two, three, four, five, six, seven) {
			quotes.push(...one);
			quotes.push(...two);
			quotes.push(...three);
			quotes.push(...four);
			quotes.push(...five);
			quotes.push(...six);
			quotes.push(...	seven);
			writeToFile(quotes);

		}




	)



	// pullQuotes().then((quote) => {

	// 	console.log('quotes', quote)

	// 	//var result = condensed.match(/[^\.!?]+[\.!\?]+/g);
	// 	writeToFile(quote)

	// });
	// for(let i = 1; i < 11; i++) {
		



	// }

}



function pullQuotes(pageNumber) {

	return new Promise((resolve) => {
		request(firstUrl + pageNumber, function(error, response, html) {
			if(!error) {

				
				let quotes = [];
				let $ = cheerio.load(html);
				$('.quoteText').each(function(i, element) {
					var a = $(this).first().text();

					var condensed = a.substr(0, a.indexOf('―'));

					condensed = condensed.replace('...', '').replace('“','').trim();
					
					var result = condensed.match(/[^\.!?]+[\.!\?]+/g);

					if(result !== null)
						quotes.push(...result);

				});

				resolve(quotes);
			}


		})
	})
	
}


function writeToFile(string) {
	fs.appendFile('messages.txt', JSON.stringify(string), function(err) {
		if(err) throw err;
	});
}