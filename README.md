hi this is a twitterbot

1. git clone this repo `git clone https://gitlab.com/thomas.lombardi/twitterbug.git`
2. `cd twitterbug`
3. `npm install`
4. `cp bot\bot.config.sample.js bot\bot.config.js`
5. In the `bot.config.js` file you just created, load in your Twitter info
6. `node index.js` or alternatively run it with pm2.