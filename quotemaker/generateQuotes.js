'use strict';


var fs = require('fs');
var Promise = require('bluebird');

const MAX_LENGTH = 140;
const DIAL_IT_IN = 125;
var terminals = {};
var startwords = [];



function returnGeneratedQuote(stats) {    
    buildStartWords(stats);
    return new Promise((resolve) => {                   
        resolve(makeTweet(stats));
    });
}


function buildStartWords(wordstats) {
    for(var prop in wordstats) {
        startwords.push(prop);
    }
}


function choice(a) {
    var i = Math.floor(a.length * Math.random());
    return a[i];
}


function makeTweet(wordstats) {
    let word = choice(startwords);        
    let title = word;
    while (wordstats.hasOwnProperty(word)) {
        let next_words = wordstats[word];
        word = choice(next_words);
        title += ' ' + word;
        if (title.length > DIAL_IT_IN && terminals.hasOwnProperty(word)) break;
    }
    if (title.length > MAX_LENGTH) return makeTweet(wordstats);

    //return title.replace(/^"|"$/g, '');
    var other = title.replace(/[\/#"”“!$%\^&\*;:{}=\-_`~()]/g,"");       
    return other.trim();
}





module.exports = {
    returnGeneratedQuote
}


//goals, ideals, a squirt rings loaded at the entire membership of a carnation petal and pessimistic