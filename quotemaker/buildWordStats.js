
'use strict';

var fs = require('fs');
var Promise = require('bluebird');
var utils = require('../util')

var wordstats = {};
var terminals = {};
var startwords = [];


function buildWordModel(arrayedText, destinationMap) {
	return new Promise((resolve) => {
        buildWordStats(arrayedText).then((words) => {
            console.log('done building')
            utils.writeToFile(destinationMap, words).then(() => {
                resolve('success'); 
            });
        });    
    })
     
}



function buildWordStats(titles) {
    
    return new Promise((resolve) => {
        for (var i = 0; i < titles.length; i++) {            

            var words = titles[i].split(' ');
            terminals[words[words.length-1]] = true;
            startwords.push(words[0]);
            for (var j = 0; j < words.length - 1; j++) {
                if (wordstats.hasOwnProperty(words[j])) {
                    wordstats[words[j]].push(words[j+1]);
                } else {
                    wordstats[words[j]] = [words[j+1]];
                }
            }

            if(i === titles.length -1 ) {
                resolve(wordstats);
            }
        }
    })
    

    
}




module.exports = {
    buildWordModel
}